## Vehicle Finder

This vehicle finder is a simple vehicle search wherein user can search a keyword or terms that they know for the car that they are looking for.

## Requirements:
- PHP 7+
- Docker, Docker-compose
- Composer
- Elasticsearch


## Installation
- Clone the repository

      git clone https://madzmar25@bitbucket.org/madzmar25/cloudemployee.git

- Navigate inside the project folder <br/><br/>

      cd ~/<project_folder>/exam

- Type in 

      composer install

- Copy the vehicles.csv into 

      <project>/exam/public

- Navigate inside the project folder 

      cd ~/<project_folder>

- Run 

      docker-compose up -d

- Modify /etc/hosts file and include this line
      
      127.0.0.1       php-docker.local

- Open browser and type
      
      http://php-docker.local:9090

  
## URLS:
**Homepage**

      http://php-docker.local:9090

**To populate elasticsearch**

      http://php-docker.local:9090/populate

**Delete the index**

      http://php-docker.local:9090/delete-index

**Check index exist**

      http://php-docker.local:9090/check-index

**Search the result**

      http://php-docker.local:9090/search
This will return in json format with the view. This is use in ajax request


### Possible issues
- If an error encountered check the following folder

  - **<project>/storage** and **<project>/bootstrap** - Make sure this is writable for the logs
  - You can use **tail -f <project>/storage/logs/*** - to check on the error.

##### Developer
    Madzmar A. Ullang [madzmar.ullang@gmail.com]
    