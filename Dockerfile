FROM php:7-fpm
COPY ./exam /var/www/html/exam
RUN apt-get update && apt-get install vim -y
RUN apt-get install git -y
WORKDIR /var/www/html/exam