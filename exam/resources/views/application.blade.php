<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>CloudEmployee Exam</title>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  @include('partials.scripts')
  @include('partials.styles')
  @yield('page-style') {{-- If we have a specific style on the specific page only --}}
</head>
<body>  
  <div class="wrapper">    
    <div id="main_container" style="position: relative">      
      @yield('content')      
    </div>        
    
  </div>
  @include('partials.footer.scripts')
</body>
</html>
