
@foreach($results as $result)
	<tr>
		<td>{{ $result["_source"]["vehicle_id"] }}</td>
		<td>{{ $result["_source"]["make"] }}</td>
		<td>{{ $result["_source"]["short_model"] }}</td>
		<td>{{ $result["_source"]["long_model"] }}</td>
		<td>{{ $result["_source"]["trim"] }}</td>
		<td>{{ $result["_source"]["derivative"] }}</td>
		<td>{{ $result["_source"]["year"] }}</td>
		<td>{{ $result["_source"]["discontinued"] }}</td>
		<td>{{ $result["_source"]["available"] }}</td>
	</tr>
@endforeach
