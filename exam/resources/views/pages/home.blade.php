@extends('application')
@section('content')
	

	<div class="container">
		<div class="row"><strong>Search for your Cars</strong></div>

		{{--- <div class="row">
			<div class="col-sm-4">
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="inputGroupSelect01">Year</label>
				  </div>
				  <select class="custom-select" id="inputGroupSelect01">
				    <option selected>Choose...</option>
				    <option value="1">One</option>
				    <option value="2">Two</option>
				    <option value="3">Three</option>
				  </select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="inputGroupSelect01">Make</label>
				  </div>
				  <select class="custom-select" id="inputGroupSelect01">
				    <option selected>Choose...</option>
				    <option value="1">One</option>
				    <option value="2">Two</option>
				    <option value="3">Three</option>
				  </select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="inputGroupSelect01">Model</label>
				  </div>
				  <select class="custom-select" id="inputGroupSelect01">
				    <option selected>Choose...</option>
				    <option value="1">One</option>
				    <option value="2">Two</option>
				    <option value="3">Three</option>
				  </select>
				</div>
			</div>
		</div>
		---}}
		<div class="row">
			<div class="input-group mb-3 mt-3">
				<div class="input-group-prepend">
					<button class="btn btn-outline-secondary" type="button" id="search">Search</button>
				</div>
				<input type="text" class="form-control" placeholder="" id="search_field" name="q" aria-label="" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="row">
			<table id="result_table" class="table table-responsive table-striped">
				<thead>
					<tr>
						<th>Vehicle ID</th>
						<th>Make</th>
						<th>Short Model</th>
						<th>Long Model</th>
						<th>Trim</th>
						<th>Derivative</th>
						<th>Year Introduced</th>
						<th>Year Discontinued</th>
						<th>Currently Available</th>
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$("#search").on('click', function() {
				$.ajax({
		            url: "{{ route('search') }}",
		            data: {
		                q: $('#search_field').val(),
		                _token: "{{ csrf_token() }}"
		            },
		            dataType: 'json',
		            type: 'POST',
		            success: function(response) {
		            	console.log(response);

		                if (response.success) {		                    
		                    $("#result_table tbody").html(response.view);
		                } else {		                	
		                    alert("Not success");
		                }

		            }
		        });
		        return false;
			});
		});
	</script>
@endsection