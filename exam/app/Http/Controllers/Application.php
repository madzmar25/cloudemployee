<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Classes\Pages;
use App\Http\Controllers\Classes\Elasticsearch;
use Elasticsearch\ClientBuilder;


/**
 * Class Application
 *
 * Handles the default application. This is the homepage
 *
 * @package App\Http\Controllers
 */
class Application extends Controller
{
	protected $client;

	use Pages, Elasticsearch;	

	public function __construct()
	{
		// Initialize the elastic search client
		$this->client = ClientBuilder::create()->setHosts(['elk'])->build();
	}    
}
