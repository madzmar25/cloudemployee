<?php
/**
 * Created by Madzmar Ullang.
 * User: madzmar
 */
namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;


trait Elasticsearch
{
	protected $index = "exam"; // This should be changed and placed in the config file
	protected $type = "cars"; // This should be changed and placed in the config file

	/**
	 * Search for the possible match of the key based on the text field
	 *
	 * @param $request - Contains the 'q' parameter. This 'q' contains the search string
	 * @return json - {
	 *                   view: '<contains an html for of response in a table format>',
	 *  				 success: 'true' - The status of the request
	 *                 }
	 */
	public function search(Request $request)
	{	
	
		$params = [
		    'index' => $this->index,
		    'type' => $this->type,
		    'size' => 100,
		    'scroll' => "10s",
		    'body' => [
		        "query" => [
			        "query_string" => [
			        	"fields" => ["make", "short_model", "long_model", "derivative", "trim"],
			        	"query" => $request->get("q"),
			        ]
			    ]
		    ]
		];

		$results = $this->client->search($params);		
		$hits = array();

		if (isset($results["hits"]["hits"]) && $results["hits"]["hits"]) {
			$hits = $results["hits"]["hits"];
		}

        return response()
            ->json([
                'view' => View::make('pages.search_table',
                    [
                        'results' => $hits,
                    ])->render(),
                'success' => true,
            ]);
	}
 
 	/**
 	 *
 	 * This function create an index in the elastic search if it does not exist and populate all the records as document
 	 *
 	 * @param $request - Contains the request parameter. But right now this is not yet used (maybe in the future enhancement)
 	 * @return json - {
	 *	
	 *					"create_index" => '<response of index creation in array format>',
  	 *					"create_document" => '<response of document create in array format>',
	 *
 	 *				 }
 	 *
 	 */
 	public function index(Request $request) 
 	{ 		
 		$response = [];

 		if (!$this->client->indices()->exists(['index' => $this->index,])) {

	 		$params = [
	 			"index" => $this->index,
	 			
	 			"body" => [
	 				"settings" => [
	 					"number_of_shards" => 2,
	 					"number_of_replicas" => 0,
	 				],
	 				"mappings" => [
	 					$this->type => [
	 						"_source" => [
	 							"enabled" => true,
	 						],
	 						"properties" => [
	 							"vehicle_id" => [
	 								"type" => "integer"
	 							],
	 							"make" => [
	 								"type" => "text",
	 							],
	 							"short_model" => [
	 								"type" => "text",
	 							],
	 							"long_model" => [
	 								"type" => "text",
	 							],
	 							"trim" => [
	 								"type" => "text",
	 							],
	 							"derivative" => [
	 								"type" => "text",
	 							],
	 							"year" => [
	 								"type" => "integer",
	 							],
	 							"discontinued" => [
	 								"type" => "integer",
	 							],
	 							"available" => [
	 								"type" => "text",
	 							],
	 						]
	 					]
	 				],
	 			],
	 		];

	 		$response = $this->client->indices()->create($params);
	 	}

 		$create_response = $this->createDocument($this->client, $request);

 		return response()
            ->json([
                "create_index" => $response,
 				"create_document" => $create_response,
            ]);
 	}

	/**
 	 *
 	 * Delete an index.
 	 *
 	 * @param $request - Contains the request parameter. But right now this is not yet used (maybe in the future enhancement)
 	 * @return json - {
	 *	
	 *					"response" => '<response of delete index in array format>',
  	 *					"status" => '<status of delete index in array format>',
	 *
 	 *				 }
 	 *
 	 */
 	public function deleteIndex(Request $request) 
 	{
 		$params = ['index' => $this->index];
		$response = $this->client->indices()->delete($params);
		
		return response()
            ->json([
                "response" => $response,
 				"status" => true,
            ]);
 	}

	/**
 	 *
 	 * Check the index. The index is set at the top of this class and by default it's using exam as index and cars as type
 	 *
 	 * @param $request - Contains the request parameter. But right now this is not yet used (maybe in the future enhancement)
 	 * @return json - {
	 *	
	 *					"response" => 'response of the checkstatus',
  	 *					"status" => 'the response status in boolean form',
	 *
 	 *				 }
 	 *
 	 */
 	public function checkIndex(Request $request)
 	{
 		if (!$this->client->indices()->exists(['index' => $this->index,])) {
 		
 			return response()
	            ->json([
	                "response" => "Index does not exist",
	 				"status" => true,
	            ]);
 		}

 		return response()
            ->json([
                "response" => 'Index exists',
 				"status" => true,
            ]);
 	}

 	/**
 	 *
 	 * Populate the needed document in the index 'exam' and type 'cars'.
 	 *
 	 * @param $request - Contains the request parameter. But right now this is not yet used (maybe in the future enhancement)
 	 * @return $response - The status of document creation
 	 *
 	 */
 	private function createDocument($client, Request $request) 
 	{ 		
 		$handle = fopen("vehicles.csv", "r");
 		$header = true;
 		$content = array();
 		$count = 1;
 		$limit = 1000;
 		$responses = [];


 		while ($csv_line = fgetcsv($handle, 1000, ",")) {
		    if ($header) {
		        $header = false;
		    } else {

		    	if ($count <= $limit) {
			    	$content['body'][] = [
			    		'index' => [
			    			'_index' => $this->index,
			    			'_type' => $this->type,
			    			'_id' => $count,
			    		]
			    	];

			    	$content['body'][] = [
			    		"vehicle_id" => $csv_line[0],
						"make" => $csv_line[1],
						"short_model" => $csv_line[2],
						"long_model" => $csv_line[3],
						"trim" => $csv_line[4],
						"derivative" => $csv_line[5],
						"year" => $csv_line[6],
						"discontinued" => $csv_line[7],
						"available" => $csv_line[8],
			    	];
			    	$count++;
		    	} else {
		    		// dd($content);
		    		$response = $this->client->bulk($content);
		    		$content = ['body' => []];
		    		// dd($response);
		    		$responses[] = $response;
		    		unset($response);
		    		$count = 0;		    		
		    	}


		    	
		    }
		}

		if (!empty($content['body'])) {
		    $response = $this->client->bulk($content);
		    $responses[] = $response;
		}

		return $responses;
 	}
}