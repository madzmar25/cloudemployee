<?php
/**
 * Created by Madzmar Ullang.
 * User: madzmar
 * Date: 17/4/17
 * Time: 5:15 PM
 */
namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;


trait Pages
{
	/**
	 * Display the home page
	 *
	 * @param $request - Contain the method parameter.
	 * @return View - Returns the HTML view of the pages
	 */ 
    public function home(Request $request)
    {
        try {
            return view('pages.home');
        } catch(Exception $e) {
            return view('errors.404');
        }
    }
}